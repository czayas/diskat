#!/usr/bin/env python
#-*- coding: iso-8859-1 -*-

'''
Diskat - Catalogador de discos - Versión 1.3 (24/07/2013) Python 2.7.4
         Copyleft 7/11/2011 Carlos Zayas Guggiari <carlos@zayas.org>

Diskat es un sencillo catalogador de medios de almacenamiento que crea listas
de los archivos contenidos tanto en la carpeta personal del usuario como también
en todos los dispositivos montados.

Las listas son archivos de texto almacenados en la carpeta personal del usuario
dentro de "~/.diskat" (carpeta oculta) y pueden ser revisados con los comandos
cat, grep, less, etc. además de con el mismo diskat.

Este programa fue probado en GNU/Linux, pero debería funcionar en cualquier otro
sistema UNIX. Para mayor comodidad, se recomienda copiarlo en /usr/bin y
convertirlo en ejecutable con el comando chmod +x

Uso:  diskat update [home] [dispositivo(s)]  Actualiza catálogo
      diskat list                            Lista dispositivos catalogados
      diskat mounts                          Lista dispositivos montados
      diskat [find] texto1 [texto2 ...]      Consulta catálogo
'''

# -----------------------------------------------------------------------------
# Importación de módulos
# -----------------------------------------------------------------------------

import os, sys

# -----------------------------------------------------------------------------
# Variables principales
# -----------------------------------------------------------------------------

home = os.path.expanduser('~')                  # Carpeta personal
args = sys.argv                                 # Argumentos de línea
name = args.pop(0).split('/')[-1].split('.')[0] # Nombre de la aplicación
base = os.path.join(home,'.'+name)              # Nombre del catálogo

# -----------------------------------------------------------------------------
# Definición de funciones generales
# -----------------------------------------------------------------------------

def mostrar(cadena, lugar='stdout'):

    'Muestra una cadena en el lugar especificado.'

    if lugar == 'stdout':
        print(cadena)

# -----------------------------------------------------------------------------

def fixpath(cadena):

    'Retorna una trayectoria con caracteres especiales corregidos.'

    fixchar = {
               '\\040':' ' # Hasta ahora sólo tuve problemas con este caracter
              }

    for char in fixchar:
        if char in cadena:
            cadena = cadena.replace(char,fixchar[char])

    return cadena

# -----------------------------------------------------------------------------

def montajes():

    'Retorna un diccionario de volúmenes montados.'

    volumen = {}

    for linea in file('/proc/mounts'):
        if linea[0] == '/':
            linea = linea.split()
            volumen[linea[0]] = fixpath(linea[1])

    return volumen

# -----------------------------------------------------------------------------

def dicvalor(dic, valor):

    'Retorna diccionario con elementos de otro que contienen un valor.'

    dicv = {}

    for clave in dic.keys():
        if valor in dic[clave]: dicv[clave] = dic[clave]

    return dicv

# -----------------------------------------------------------------------------

def dicdiscos():

    'Retorna diccionario de dispositivos catalogables.'

    discos = montajes()

    discos['home'] = home # Se agrega la carpeta personal del usuario

    borrar = ''

    for item in discos:
        if discos[item] == '/': borrar = item

    if borrar: discos.pop(borrar) # Se quita la raiz del sistema de archivos

    return discos

# -----------------------------------------------------------------------------
# Definición de funciones de usuario
# -----------------------------------------------------------------------------

def dk_update():

    'Actualización del catálogo.'

    discos = dicdiscos()

    args.pop(0)

    if args: # Actualizar sólo uno o algunos montajes

        nuevo = {} # Nuevo diccionario de discos que actualizar

        for arg in args:
            pre = ''
            if arg != 'home': pre = '/dev/'
            if discos.has_key(pre+arg):
                nuevo[pre+arg] = discos[pre+arg]
            else:
                nuevo.update(dicvalor(discos,arg)) # Agrega diccionario

        discos = nuevo

    for disco in discos:

        mostrar('Catalogando: '+disco+' -> '+discos[disco])

        catalogo = os.path.join(base,os.path.basename(discos[disco]))

        diskat = open(catalogo,'w') # Sobreescribe el catálogo anterior

        for root, dirs, files in os.walk(discos[disco]):

            for item in files:
                diskat.write(os.path.join(root,item)+'\n')

        diskat.close()

# -----------------------------------------------------------------------------

def dk_list():

    'Listado de dispositivos catalogados.'

    for item in os.listdir(base): mostrar(item)

# -----------------------------------------------------------------------------

def dk_mounts():

    'Listado de dispositivos montados.'

    discos = dicdiscos()
    claves = discos.keys()
    claves.sort()

    for item in claves:
        if item != 'home': mostrar(item+' -> '+discos[item])

# -----------------------------------------------------------------------------

def dk_find():

    'Consulta del catálogo.'

    if args[0] == 'find': args.pop(0)

    if args:

        for archivo in os.listdir(base):

            catalogo = os.path.join(base,archivo)

            diskat = open(catalogo)

            for linea in diskat:

                ok = True

                for item in args:
                    if not item.lower() in linea.lower(): ok = False

                if ok: print linea[:-1]

            diskat.close()

# -----------------------------------------------------------------------------

def dk_help():

    'Despliegue de la documentación'

    mostrar(__doc__[1:-1]) # Suprime las líneas vacías del inicio y del final

# -----------------------------------------------------------------------------
# Función principal
# -----------------------------------------------------------------------------

def main():

    'Función principal.'

    if args:

        if args[0] == 'update':
            dk_update()

        elif args[0] == 'list':
            dk_list()

        elif args[0] == 'mounts':
            dk_mounts()

        elif args[0] == 'help':
            dk_help()

        else:
            dk_find()

    else:
        mostrar(name+': Faltan parámetros. Para ayuda, '+name+' help <Enter>')

# -----------------------------------------------------------------------------
# Creación de carpeta de catálogos
# -----------------------------------------------------------------------------

if os.path.exists(base):
    if os.path.isfile(base): os.remove(base)

if not os.path.exists(base): os.makedirs(base)

# -----------------------------------------------------------------------------
# Si no se importa como módulo, se ejecuta la función principal
# -----------------------------------------------------------------------------

if __name__ == '__main__': main()
