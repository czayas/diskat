# Diskat

## Catalogador de discos - Versión 1.3 (24/07/2013) Python 2.7.4

### Copyleft 7/11/2011 Carlos Zayas Guggiari <carlos@zayas.org>

Diskat es un sencillo catalogador de medios de almacenamiento que crea listas
de los archivos contenidos tanto en la carpeta personal del usuario como también
en todos los dispositivos montados.

Las listas son archivos de texto almacenados en la carpeta personal del usuario
dentro de "~/.diskat" (carpeta oculta) y pueden ser revisados con los comandos
cat, grep, less, etc. además de con el mismo diskat.

## Instalación

Este programa fue probado en GNU/Linux, pero debería funcionar en cualquier otro
sistema UNIX. Para mayor comodidad, se recomienda copiarlo en /usr/bin y
convertirlo en ejecutable con el comando chmod +x

## Uso:

```bash
    diskat update [home] [dispositivo(s)] # Actualiza catálogo
    diskat list                           # Lista catálogos
    diskat mounts                         # Lista dispositivos montados
    diskat [find] texto1 [texto2 ...]     # Consulta catálogo
```
